﻿namespace FlatsCRM.DAL.EF.Models
{
    public class Price
    {
        public double PriceForRent { get; set; }
        public double PriceForSell { get; set; }
    }
}
