﻿using FlatsCRM.DAL.EF.Enums;
using System.ComponentModel.DataAnnotations;

namespace FlatsCRM.DAL.EF.Models
{
    public class FlatProperies
    {
        [Key]
        public int Id { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public int BuildingNumber { get; set; }
        public int? FlatNumber { get; set; }
        public Price Price { get; set; }

        public string WhenFree { get; set; }
        public string RentPeroid { get; set; }
        public double Area { get; set; }
        public int Floor { get; set; }
        public int RoomNumbers { get; set; }
        public int NumberOfLevels { get; set; }
        public StandardType StandardType { get; set; }
        public InstalationStatus InstalationStatus { get; set; }
        public LoudnessType Loudness { get; set; }
        public KitchenType KitchenType { get; set; }
        public State BathState { get; set; }
        public WindowType WindowsType { get; set; }

        public FlatType FlatType { get; set; }
        public int NumberOfFlors { get; set; }
        public BuildingMaterialType BuildingMaterialType { get; set; }
        public int YearOfBuilding { get; set; }

        public GarageType GarageType { get; set; }

        public ContactPerson ContactPerson { get; set; }
        public FlatOwner FlatOwner { get; set; }
    }
}
