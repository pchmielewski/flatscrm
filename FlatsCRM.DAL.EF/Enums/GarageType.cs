﻿namespace FlatsCRM.DAL.EF.Enums
{
    public enum GarageType
    {
        NotExist,
        ParkingPlaceNearBuilding,
        UndergroundPlace
    }
}