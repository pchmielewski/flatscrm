﻿namespace FlatsCRM.DAL.EF.Enums
{
    public enum State
    {
        Verylow,
        Low,
        Medium,
        High,
        VeryHigh
    }
}