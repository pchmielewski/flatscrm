﻿namespace FlatsCRM.DAL.EF.Enums
{
    public enum LoudnessType
    {
        LowLoud,
        MediumLoud,
        HighLoud
    }
}