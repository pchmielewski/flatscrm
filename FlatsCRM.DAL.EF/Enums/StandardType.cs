﻿namespace FlatsCRM.DAL.EF.Enums
{
    public enum StandardType
    {
        LowStandard,
        MediumStandard,
        HighStandard
    }
}