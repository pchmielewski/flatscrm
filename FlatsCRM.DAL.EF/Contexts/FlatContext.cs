﻿using System;
using FlatsCRM.DAL.EF.Models;
using System.Data.Entity;

namespace FlatsCRM.DAL.EF.Contexts
{
    public class FlatContext : DbContext
    {

        public DbSet<FlatProperies> FlatProperies { get; set; }

        public FlatContext() : base(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\pawel_000\Documents\visual studio 2015\Projects\FlatsCRM\FlatsCRM.DAL.EF\Contexts\FlatsCRM_alfa.mdf;Integrated Security=True")
        {
   
        }


#warning Update to EF 7
        /*
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // Visual Studio 2015 | Use the LocalDb 12 instance created by Visual Studio
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=FlatsCRM;Trusted_Connection=True;");

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //https://github.com/aspnet/EntityFramework/wiki/Using-EF7-in-Traditional-.NET-Applications
            base.OnModelCreating(modelBuilder);
        }*/
    }
}
