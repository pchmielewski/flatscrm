﻿using System.ComponentModel.DataAnnotations;

namespace FlatsCRM.DAL.EF.Abstracts
{
    public abstract class Person
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
